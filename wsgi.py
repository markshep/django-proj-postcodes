import django.core.wsgi
import os
import sys


sys.path.append(os.path.normpath(__file__ + "/../.."))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "postcodes.settings")
application = django.core.wsgi.get_wsgi_application()
