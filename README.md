# Overview

This Django project is a web app which displays UK postcodes on an
interactive map.

The postcode data is managed and served up by
[django-app-codpo](https://bitbucket.org/markshep/django-app-codepo)
and originates from Ordnance Survey's [Code-Point
Open](http://www.ordnancesurvey.co.uk/oswebsite/products/code-point-open/)
data.

The code in this project is licensed under [GNU
AGPLv3](http://www.gnu.org/licenses/agpl-3.0.html).

A live example is at <http://bn1.net/>.
