from django.conf.urls import patterns, include, url
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
    # local views
    url(r"^$", "postcodes.views.index"),

    # other Django app views - these are AJAX lookups returning JSON
    url(r"^markers$", "codepo.views.markers"),
    url(r"^search$", "codepo.views.search"),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
