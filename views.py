# core Django imports
from django.conf import settings
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, render_to_response

# other Django app imports
import codepo.models


def index(request):
    server = request.META["SERVER_NAME"]
    format = request.COOKIES.get("format")
    if format is None:
        if " Android " in request.META.get("HTTP_USER_AGENT", ""):
            format = "mobile"
        else:
            format = "desktop"
    page_data = {
        "debug": settings.TEMPLATE_DEBUG,
        "format": format,
        }
    return render_to_response("index.html", page_data)
