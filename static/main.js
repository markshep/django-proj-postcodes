// Depends on jQuery and jQuery cookie plugin
// Depends on Leaflet and Leaflet MarkerCluster plugin


var max_zoom = 18;
var map;
var request;
var request_bounds;
var markers;
var markers_bounds;
var closest_postcode;
var closest_point;
var popup;
var popup_postcode;
var detached_popup_postcode;
var debug_messages = [];
var ignore_next_hashchange = false;
var ward_shown;
var ward_postcode;
var debug = true;


function zeropad(number, len)
{
    var str = number.toString();
    while (str.length < len)
    {
        str = "0" + str;
    }
    return str;
}


function log(text)
{
    if (debug)
    {
	var len = debug_messages.length;
	var max_messages = 30;
	if (len >= max_messages)
	{
            debug_messages = debug_messages.slice(0, max_messages - 1);
	}
	var now = new Date();
	var hours = zeropad(now.getHours(), 2);
	var mins = zeropad(now.getMinutes(), 2);
	var secs = zeropad(now.getSeconds(), 2);
	var milli = zeropad(now.getMilliseconds(), 3);
	var time = hours + ":" + mins + ":" + secs + "." + milli;
	debug_messages.splice(0, 0, "<font size=\"-2\">[" + time + "]</font> " + text);
	$("#debug").html(debug_messages.join("<br />"));
    }
}


function set_format(format)
{
    $.cookie("format", format);
    location.reload();
}


function orientation()
{
    window.scrollTo(0, 1);
}


function set_busy(flag)
{
    if (flag == true)
    {
        $("#spinner").css("visibility", "visible");
    }
    else
    {
        $("#spinner").css("visibility", "hidden");
    }
}


function replace_markers(new_markers, bounds)
{
    if (map._popup && map._popup._source)
    {
        // we detach this popup from its existing marker that's about
        // to be deleted so it doesn't disappear
        detached_popup_postcode = map._popup._source.options.title;
        log("Detaching popup for " + detached_popup_postcode);
        map._popup._source._popup = null;
        map._popup._source = null;
    }

    if (markers != null)
    {
        map.removeLayer(markers);
    }
    if (new_markers != null)
    {
        map.addLayer(new_markers);
    }
    if (markers != null)
    {
        for (var postcode in markers)
        {
            delete markers[postcode];
        }
        delete markers;
        markers = null;
    }
    markers = new_markers;
    markers_bounds = bounds;
}


function set_popup(postcode, point, info)
{
    if (popup && (popup_postcode.localeCompare(postcode) != 0))
    {
        map.removeLayer(popup);
        popup = null;
        popup_postcode = null;
    }

    if (postcode)
    {
        if (popup == null)
        {
            var content = "Postcode: " + postcode + "<br />" + info;
            popup = L.popup().setLatLng(point).setContent(content).openOn(map);
            popup_postcode = postcode;
        }
        else
        {
            popup.openOn(map);
        }
    }
}


function hash_change()
{
    // this zooms the map fully in and centres it on the postcode
    // specified in the hash of the current URL
    if (ignore_next_hashchange)
    {
        ignore_next_hashchange = false;
        return;
    }
    var postcode = clean_postcode(window.location.hash, " ");
    if (postcode.length)
    {
        var point;
        if (postcode == closest_postcode)
        {
            point = closest_point;
        }
        else if (postcode.length && markers)
        {
            markers.eachLayer(function(marker) {
                if (marker.options.title == postcode)
                {
                    point = marker._latlng;
                }
            });
        }

        if (point)
        {
            map.panTo(point);
            map.off("zoomstart", clear_hash);
            map.setZoom(max_zoom);
            map.on("zoomstart", clear_hash);
        }
    }
}


function set_closest(postcode, distance, marker, point)
{
    var max_distance = 10.0;
    if (postcode)
    {
        log("Setting closest postcode to " + postcode);
        var hash = postcode.replace(" ", "+");
        var anchor = $("<a href=\"#" + hash + "\">" + postcode + "</a>");
        if ((map.getZoom() == max_zoom) && (distance <= max_distance))
        {
            // we can't temporarily unbind the hashchange handler
            // because the actual implementation runs off a timer in
            // the background
            ignore_next_hashchange = true;
            window.location.hash = hash;

            // show the ward information for this postcode
            if (!ward_shown)
            {
                if (ward_postcode == postcode)
                {
                    show_ward();
                }
                else
                {
                    get_ward(postcode);
                }
            }

            // show the popup for this postcode marker
            if (map._popup && (detached_popup_postcode == postcode))
            {
                log("Already showing popup for " + postcode);
            }
            else
            {
                detached_popup_postcode = null;
                log("Showing popup for " + postcode);
                marker.openPopup();
            }
        }
        $("#closest-postcode").html(anchor);
        $("#closest-distance").text(distance.toFixed(2) + "m");
    }
    else
    {
        $("#closest-postcode").text("N/A");
        $("#closest-distance").text("N/A");
    }
    closest_postcode = postcode;
    closest_point = point;
    if (!distance || (distance > max_distance) || (map.getZoom() != max_zoom))
    {
        clear_hash();
        hide_ward();
    }
    else if (ward_shown && (ward_postcode != postcode))
    {
        hide_ward();
    }
}


function add_markers(data)
{
    // AJAX request complete
    set_busy(false);
    request = null;

    // process the new markers
    var count = data.length;
    var max_postcodes = 5000;
    if (count > max_postcodes)
    {
        log("Too many postcodes! (" + count + " > " + max_postcodes + ")");
        replace_markers();
        set_closest();
    }
    else
    {
        log("Got " + count + " postcodes");
        var new_markers = new L.MarkerClusterGroup();
        var centre = map.getCenter();
        var closest_postcode;
        var closest_distance;
        var closest_marker;
        var closest_point;
        for (var i = 0; i < count; ++i)
        {
            var item = data[i];
            var postcode = item[0];
            var point = L.latLng(item[1], item[2]);
            var hash = postcode.replace(" ", "+");
            var content = "<a href=\"#" + hash + "\">" + postcode + "</a>";
            var marker = L.marker(point, {title: postcode}).bindPopup(content);
            new_markers.addLayer(marker);
            var distance = centre.distanceTo(point);
            if ((closest_postcode == null) || (distance < closest_distance))
            {
                closest_postcode = postcode;
                closest_distance = distance;
                closest_marker = marker;
                closest_point = point;
            }
        }

        // update the display
        replace_markers(new_markers, request_bounds);
        set_closest(closest_postcode, closest_distance, closest_marker, closest_point);
    }
}


function cancel_request()
{
    // cancels any oustanding AJAX request for postcodes
    if (request)
    {
        request.abort();
        request = null;
        set_busy(false);
        log("Aborted postcode request");
    }
}


function update()
{
    // start loading postcode markers for the current map region and surrounds
    cancel_request();
    var centre = map.getCenter();
    $("#centre").text(centre.lat.toFixed(4) + ", " + centre.lng.toFixed(4));
    $("#bounds").text(map.getZoom());
    if (map.getZoom() < 13)
    {
        replace_markers();
        set_closest();
    }
    else
    {
        var bounds = map.getBounds();
        var width = bounds.getNorthEast().lng - bounds.getSouthWest().lng;
        var height = bounds.getNorthEast().lat - bounds.getSouthWest().lat;
        var bbox = {
            "x1": bounds.getSouthWest().lng - width,
            "y1": bounds.getSouthWest().lat - height,
            "x2": bounds.getNorthEast().lng + width,
            "y2": bounds.getNorthEast().lat + height,
            }
        request = $.get("markers", bbox, add_markers);
        request_bounds = L.latLngBounds([[bbox["y1"], bbox["x1"]], [bbox["y2"], bbox["x2"]]]);
        if (!markers_bounds || !markers_bounds.contains(request_bounds))
        {
            // don't show as busy if we're requesting a subset of the data we already have
            set_busy(true);
        }
        log("Requesting postcodes...");
    }
}


function hide_ward()
{
    $("#ward").hide();
    ward_shown = null;
}


function show_ward()
{
    log("Showing ward for " + ward_postcode);
    $("#ward").show();
    ward_shown = true;
}


function set_ward_data(data)
{
    var html = "The centre point of " + data["postcode"] + " is in "
        + data["ward"];
    var county = data["county"];
    var district = data["district"];
    if (county)
    {
        html += ", " + district + ", " + county;
    }
    else
    {
        // a Unitary Authority
        html += " in " + district;
    }
    $("#ward").html(html);
    ward_postcode = data["postcode"];
    show_ward();
}


function get_ward(postcode)
{
    log("Getting ward for " + postcode);
    $.get("search", clean_postcode(postcode, "+"), set_ward_data);
}


function popup_open(e)
{
    if (e.popup._source.options.title == ward_postcode)
    {
        show_ward();
    }
}


function search_result(data)
{
    set_busy(false);
    if (data == null)
    {
        log("Postcode not found");
    }
    else
    {
        var postcode = data["postcode"]
        log("Postcode " + postcode + " found");
        var point = L.latLng(data["lat"], data["lon"]);
        map.panTo(point);
        map.setZoom(max_zoom);
        set_ward_data(data);
    }
}


function do_search()
{
    var postcode = clean_postcode($("#search").val(), "+");
    if (postcode)
    {
        log("Searching for " + postcode);
        set_busy(true);
        $.get("search", postcode, search_result).fail(function (){
            search_result(null);
        });
    }
    // need this are we're called upon form submission
    return false;
}


function clean_postcode(postcode, separator)
{
    var compact = postcode.replace("#", "").
        replace("+", "").replace(" ", "").toUpperCase();
    var len = compact.length;
    if ((len == 6) || (len == 7))
    {
        return compact.substr(0, len - 3) +
            (separator? separator : "") + compact.substr(len - 3, 3);
    }
    else
    {
        return "";
    }
}


function clear_hash()
{
    window.location.hash = "";
}


function initialise(data)
{
    set_busy(false);
    if (data == null)
    {
        log("Postcode not found");
    }
    else
    {
        var postcode = data["postcode"];
        log("Postcode " + postcode + " found");
        var point = L.latLng(data["lat"], data["lon"]);
        map.panTo(point);
        map.off("zoomstart", clear_hash);
        map.setZoom(max_zoom);
        map.on("zoomstart", clear_hash);
        set_ward_data(data);
    }
}


function set_credits_position()
{
    var top = $("#map").offset().top;
    var left = $("#map").offset().left;
    var right = $("#info").offset().left + $("#info").outerWidth();
    var bottom = $("#info").offset().top + $("#info").outerHeight();
    var x = (left + right - $("#credits").outerWidth()) / 2;
    var y = (top + bottom - $("#credits").outerHeight()) / 2;
    $("#credits").css("left", x + "px");
    $("#credits").css("top", y + "px");
}


function credits()
{
    var visibility = $("#credits").css("visibility");
    if (visibility == "hidden")
    {
        set_credits_position();
        $(window).bind("resize", set_credits_position);
        $("#overlay").css("visibility", "visible");
        $("#credits").css("visibility", "visible");
    }
    else
    {
        $(window).unbind("resize", set_credits_position);
        $("#overlay").css("visibility", "hidden");
        $("#credits").css("visibility", "hidden");
    }
}


$(function()
{
    // is debugging enabled?
    debug = ($("#debug").css("background-color") == "rgb(255, 255, 238)");

    // attempt to hide the address bar on Android
    $(window).bind("orientationchange", orientation);
    orientation();

    // create the map
    var mapnik = L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png");
    map = L.map("map", {
        center: L.latLng(50.8409, -0.1438),
        zoom: 16,
        minZoom: 5,
        zoomControl: false,
        attributionControl: false,
        layers: [mapnik],
    })
    L.control.scale().addTo(map);
    map.on("moveend", update);
    map.on("zoomstart", function(){
        map.closePopup();
        detached_popup_postcode = null;
    });
    map.on("popupopen", popup_open);
    map.on("popupclose", hide_ward);

    // add a spinner on top of the map
    var spinner = $("<img src=\"static/spinner.gif\" width=\"25\" height=\"25\" id=\"spinner\" />");
    $("#map").append(spinner);
    spinner.css("position", "absolute");
    spinner.css("right", "0");
    spinner.css("bottom", "0");
    spinner.css("opacity", "0.3");

    // add a div on top of the map for ward information
    var frame = $("<div id=\"frame\"></div>");
    $("#map").append(frame);
    var ward = $("<div id=\"ward\"></div>");
    frame.append(ward);
    frame.css("position", "absolute");
    frame.css("left", "5px");
    frame.css("bottom", "40px");
    frame.css("width", ($("#map").width() - 20) + "px");
    ward.css("width", "258px");
    ward.css("margin", "0 auto");
    ward.css("border", "1px solid #ccc");
    ward.css("padding", "2px 5px");
    ward.css("font-size", "11px");
    ward.css("text-align", "center");
    ward.css("background", "white");
    ward.css("opacity", "0.9");
    ward.hide();

    // setup the search submission
    $("#form").submit(do_search);

    // start loading the data for the current area
    update();

    // have we been given a postcode?
    var hash = window.location.hash;
    if (hash)
    {
        set_busy(true);
        var postcode = clean_postcode(hash, "+")
        log("Searching for " + postcode);
        window.location.hash = postcode;
        $.get("search", postcode, initialise).fail(function (){
            initialise(null);
        });
    }

    // this gets triggered when the "closest postcode" link is clicked
    $(window).bind("hashchange", hash_change);

});
